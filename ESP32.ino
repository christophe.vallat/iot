
#include <WiFi.h> //connectivité
#include <WiFiClient.h>
//#include <WiFiClientSecure.h>
#include <ESPmDNS.h> //mDNS pou les noms d'hôtes
#include <PubSubClient.h>//MQTT

#define ssid "hotspotA"
#define password "12345678"
#define myhostname "client2"


const int MQTT_PORT=1883;//1883;//8883;//port ecoute MQTT serveur //********
const char* mqtt_server = "broker.local.com";//adresse du server ip ou nom 192.168.2.17

WiFiClient espClient;
//WiFiClientSecure espClient; 

PubSubClient client(espClient);//**publication MQTT avec le nom de l'identification objet connecte
long lastMsg = 0;//pour l'intervalle
byte val = 0; //valeur à envoyer

void reconnect(){
    //connecté au broker ?
    while(!client.connected()){
      //connexion MQTT via PubSubClient change  pour intégrer l'authentification
      if(!client.connect(myhostname,"sondes", "pass")){
        Serial.print("Erreur connecion MQTT, rc= ");
        Serial.println(client.state());
        delay(5000);
        continue;
      }
      Serial.println("Connexion serveur MQTT ok");
    }
}

//on prefere réunire la partie connexion au lieu de la mettre dans le setup
//connexion wifi et config mDNS
void setup_wifi(){
//mode station
WiFi.mode(WIFI_STA);//mode infrastructure station
Serial.println();
Serial.print("Connexion:  "); Serial.println(ssid);
//connexion wifi
WiFi.begin(ssid, password);
  while(WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
//affichage
Serial.print(""); Serial.println("Connexion wifi ok");
Serial.print("Adresse IP: "); Serial.println(WiFi.localIP());
//configuration mDNS
//WiFi.hostname(myhostname);
  if(!MDNS.begin(myhostname)){
    Serial.println("Erreur configuration mDNS!");
  }else{
    Serial.print("répondeur mDNS démarré: ");
    Serial.println(myhostname);
  }

}



void setup() {
  // put your setup code here, to run once:
//configuration moniteur serie
 Serial.begin(115200);

 //configuration wifi
 setup_wifi();
 //configuration broker
 //MQTT_PORT est une macro 1883 correspond 
 //au port TCP/IP par défaut des broket MQTT
 //on reviendra par la suite
 client.setServer(mqtt_server, MQTT_PORT);

 
}

void loop() {
  // put your main code here, to run repeatedly:
char msg[16];//array pour conversion val
    char topic[64];//array pour topic
  //sommes nous connectés ?
  if(!client.connected()){
       reconnect();//non. connexion
   }


//gestion MQTT
  client.loop();

//temporisation
 long now = millis();
 if(now - lastMsg > 5000){
  //5s de passé
  lastMsg = now;
  val++;
  //construction message
 sprintf(msg, "hello world %ld", val);
   //construction topic
  sprintf(topic, "mesure2/%s/valeur", myhostname);
  Serial.print("Publish message: ");
    Serial.println(msg);
  
  //publication message sur topic
  client.publish(topic, msg);
  
 }



   
}
